# Netflim

- Suis les consignes du [brief de projet](https://gitlab.com/-/snippets/3625973).
- Travaille en [agilité avec le framework SCRUM](https://gitlab.com/-/snippets/3625937).
- Respecte les étapes [du GitLab Flow](https://gitlab.com/-/snippets/3625931).
- Crée [des demandes de fusion et procède à de la relecture de code](https://gitlab.com/-/snippets/3627568).
- Structure le projet en suivant [les bonnes pratiques](https://gitlab.com/-/snippets/3623098).