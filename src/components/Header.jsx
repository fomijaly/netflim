import "./Header.css";
import netflimLogo from "../assets/logo-netflim.svg";
import pp from "../assets/jeanmichel.jpg";
import { Link } from "react-router-dom";

function Header({searchBarValue,setSearchBarValue}) {

  function handleSubmit(event) {
    event.preventDefault();
    setSearchBarValue(searchBarValue)
  }

  return (
    <div className="header__container">
      <Link className="header__logo" to={"/"}>
          <img className="logo" src={netflimLogo} alt="Logo Netflim" />
          <h1>Netflim</h1>
      </Link>
      <form className="header__searchBar">
        <input
          type="text"
          name="search"
          placeholder="Rechercher"
          value={searchBarValue}
          onChange={(event) => setSearchBarValue(event.target.value)}
        />
        <button type="submit" onClick={handleSubmit}>
          yes
        </button>
      </form>
      <section className="header__profil">
        <a href="/account" className="header__profil-a">
          <img src={pp} alt="Photo de Profil" />
          <p>Mon compte</p>
        </a>
      </section>
    </div>
  );
}

export default Header;
