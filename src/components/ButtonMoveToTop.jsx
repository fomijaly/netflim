import topArrow from "../assets/topArrow.svg";
import "../components/ButtonMoveToTop.css";

export default function ButtonMoveToTop() {
  return (
    <a href="#top"> {/* Permet de rediriger vers le haut de la page au clic */}
      <img src={topArrow} alt="Logo de flèche montante" className="top-arrow"/>
    </a>
  );
}
