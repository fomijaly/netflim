import { useState, useEffect } from "react";

// Fonction utilitaire pour récupérer la valeur stockée dans le localStorage
function getStorageValue(key, defaultValue) {
  // Récupération de la valeur stockée
  const saved = localStorage.getItem(key);
  const initial = JSON.parse(saved);
  return initial || defaultValue;
}

// Définition du hook personnalisé useLocalStorage
export const useLocalStorage = (key, defaultValue) => {
  // Utilisation du useState pour gérer l'état de la valeur
  const [value, setValue] = useState(() => {
    // Utilisation de la fonction getStorageValue lors de l'initialisation pour obtenir la valeur du localStorage
    return getStorageValue(key, defaultValue);
  });

  // Utilisation du useEffect pour mettre à jour le localStorage chaque fois que la clé ou la valeur change
  useEffect(() => {
    // Stockage de la valeur dans le localStorage sous forme de chaîne JSON
    localStorage.setItem(key, JSON.stringify(value));
  }, [key, value]); // Le tableau de dépendances assure que le useEffect est déclenché lorsque key ou value change

  // Le hook renvoie un tableau contenant la valeur actuelle et une fonction pour la mettre à jour
  return [value, setValue];
};
