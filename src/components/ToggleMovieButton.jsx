import React from "react";
import "./MovieCard.css";
import "./ToggleMovieButton.css";

function ToggleMovieButton({
  src,
  alt,
  action,
  addToWatch,
  addToWatched,
  addToFavourites,
}) {
  return (
    <button onClick={action} className="toggleMovieButton">
      <span title={alt}>
        <img
          className={`interactive-logo 
          ${addToWatch ? "jaune" : ""} 
          ${addToWatched ? "jaune" : ""} 
          ${addToFavourites ? "jaune" : ""}`}
          src={src}
          alt={`Logo ${alt}`}
        />
      </span>
    </button>
  );
}

export default ToggleMovieButton;
