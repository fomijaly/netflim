// Fonction asynchrone pour charger les détails des films depuis l'API TMDb
export async function loadMovies() {
  // Options pour la requête HTTP
  const options = {
    method: "GET",
    headers: {
      accept: "application/json",
      Authorization:
        "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIzODc4ZWExMTkxOTI3ZGRlNjIzNWY3MzAwYzVkMDY5YiIsInN1YiI6IjY1Nzg1ZGVlODlkOTdmMDBjNjc5MDIyYyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.nVCJIptl8KEeB1SZpV5GpNMy_WSBiaPwX0kuqgpbXTk",
    },
  };

  // Envoi de la requête pour obtenir la liste de films
  const apiResponse = await fetch(
    "https://api.themoviedb.org/3/discover/movie?include_adult=false&include_video=false&language=en-US&page=1&sort_by=popularity.desc",
    options
  );

  // Extraction du contenu JSON de la réponse
  const movieList = await apiResponse.json();

  // Initialisation du tableau pour stocker les détails de tous les films
  const allMovieDetails = [];

  // Boucle à travers la liste de films pour obtenir les détails individuels
  for (const movie of movieList.results) {
    // Envoi de la requête pour obtenir les détails spécifiques du film
    const movieDetailResponse = await fetch(
      `https://api.themoviedb.org/3/movie/${movie.id}?api_key=60ce5f9c7985d0f00d208c09e9a54c8e`
    );

    // Extraction du contenu JSON de la réponse pour les détails du film
    const movieDetail = await movieDetailResponse.json();

    // Ajout des détails du film au tableau
    allMovieDetails.push(movieDetail);
  }

  // Retourne le tableau complet avec les détails de tous les films
  return allMovieDetails;
}

