// Importation de React et de la feuille de style dédiée à SeeMoreCard
import React from "react";
import "./AddMoreCard.css";
import {Link} from "react-router-dom"; // Importation du composant de navigation Link

// Définition du composant fonctionnel SeeMoreCard
export default function AddMoreCard(props) {
  // Extraction de la propriété 'number' depuis les propriétés du composant parent
  const {url} = props;

  // Retourne la structure JSX du composant SeeMoreCard
  return (
    <article className="add-more-card">
      {/* Utilisation du composant de navigation Link pour rediriger vers la liste à voir */}
      <Link to={`/`} className="image-container">
        <div className="add-more-card-content">
          <p className="add-more-card-text">Cette liste est bien vide...</p>
          <p className="add-more-card-text-add">
            Je veux y ajouter
            <br /> des films !
          </p>
        </div>
      </Link>
    </article>
  );
}
