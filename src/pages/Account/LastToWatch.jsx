import { useState, useEffect } from "react";
import MovieCard from "../../components/MovieCard";
import "./LastToWatch.css";
import SeeMoreCard from "./SeeMoreCard";
import { loadMovies } from "../../services/api";
import AddMoreCard from "./AddMoreCard";

export default function LastToWatch(props) {
  // Extraction de la fonction de mise à jour du nombre total de films "à voir" depuis les propriétés
  const { setTotalMoviesInToWatch } = props;

  // Utilisation de l'état local pour stocker la liste des films "à voir" et une fonction pour la mettre à jour
  const [movieToWatch, setMovieToWatch] = useState([]);

  // Utilisation de l'état local pour stocker la liste de films obtenue depuis l'API et une fonction pour la mettre à jour
  const [movieListApi, setMovieListApi] = useState([]);

  // Options de la requête API, notamment la clé API, la méthode et les en-têtes

  // Effet de côté qui s'exécute lors du montage du composant
  useEffect(() => {
    // Requête à l'API pour obtenir la liste des films populaires
    async function callApi() {
      const callMovieList = await loadMovies();
      // Mise à jour de la liste de films obtenue depuis l'API
      setMovieListApi(callMovieList);

      // Création d'une copie des données stockées dans le local storage
      const localStorageData = { ...localStorage };

      // Initialisation d'un tableau vide pour stocker les films à regarder
      let filmsToWatch = [];


        // Boucle sur les clés du local storage
        for (const key in localStorageData) {
          // Vérification si la valeur associée à la clé est "true"
          if (key.startsWith("to-watch") && localStorageData[key] === "true") {
            // Vérifie qu'on ne récupère que les films true de la liste to watch
            // Extraction de l'ID du film à partir de la clé
            let newKey = key.split(" ");
            newKey = newKey[1]; // garde le deuxième élément du tableau, soit l'id du film


          // Recherche du film correspondant dans la liste obtenue depuis l'API
          for (const eachMovie of callMovieList) {
            if (eachMovie.id == newKey) {
              // Ajout du film à la liste des films à regarder
              filmsToWatch.push(eachMovie);
            }
          }
        }
      }

      // Mise à jour de la liste des films à regarder et du nombre total de films à regarder
      setMovieToWatch(filmsToWatch);
      setTotalMoviesInToWatch(filmsToWatch.length);
    }
    callApi();
  }, []); // Le tableau de dépendances est vide pour exécuter l'effet une seule fois lors du montage

  // Conversion des données obtenues depuis l'API en un tableau
  const data = movieListApi.map((eachData) => eachData);

  return (
    <>
      {/* Conteneur des cartes de films à regarder */}

      {movieToWatch.length !== 0 ? (
        <div className="last-to-watch-container">
          {/* Affichage des quatre derniers films à regarder */}
          {movieToWatch.slice(-4).map((current) => (
            <MovieCard
              title={current.title}
              poster={current.poster_path}
              key={current.id}
              id={current.id}
            />
          ))}
          {/* Et à la fin des 4 cartes, on ajoute la carte "Voir plus" affichant le nombre total de films à regarder */}
          {movieToWatch.length > 4 ? (
            <SeeMoreCard number={movieToWatch.length} url="/towatchlist" />
          ) : (
            <AddMoreCard />
          )}
        </div>
      ) : (
        <p>Vous n'avez enregistré aucun film dans cette liste.</p>
      )}
    </>
  );
}
