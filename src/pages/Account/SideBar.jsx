import "./SideBar.css";
import pencil from "../../assets/pencil.svg";
import settings from "../../assets/settings.svg";
import help from "../../assets/help.svg";
import user from "../../assets/user.svg";
import logout from "../../assets/logout.svg";
import pp from "../../assets/jeanmichel.jpg";

export default function SideBar() {
  return (
    <div className="account-sidebar-container">
      <div className="account-sidebar-top-pencil-image">
        <img src={pencil} alt="Logo d'un crayon" />
      </div>
      <div className="account-sidebar-top">
        <img
          className="account-sidebar-user-photo"
          src={pp}
          alt="Photo de profil"
        />
        <h2>Jean Michel</h2>
      </div>
      <div className="account-sidebar-links">
        <div className="account-sidebar-settings-container">
          <img src={settings} alt="Logo d'un rouage" />
          <h2>Paramètres</h2>
        </div>
        <div className="account-sidebar-help-container">
          <img src={help} alt="Logo d'un point d'interrogation" />
          <h2>Aide</h2>
        </div>
        <div className="account-sidebar-user-container">
          <img src={user} alt="Logo d'une personne" />
          <h2>Coordonnées</h2>
        </div>
      </div>
      <div className="account-sidebar-logout-container">
        <img src={logout} alt="Logo d'une porte de sortie" />
        <h2>
          <strong>Déconnexion</strong>
        </h2>
      </div>
    </div>
  );
}
