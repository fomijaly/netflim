import {useState, useEffect} from "react";
import MovieCard from "../../components/MovieCard";
import "./LastToWatched.css";
import SeeMoreCard from "./SeeMoreCard";
import AddMoreCard from "./AddMoreCard";

export default function LastToWatched(props) {
  // Extraction de la fonction de mise à jour du nombre total de films "vus" depuis les propriétés
  const {setTotalMoviesInToWatched} = props;

  // Utilisation de l'état local pour stocker la liste des films "vus" et une fonction pour la mettre à jour
  const [movieToWatched, setMovieToWatched] = useState([]);

  // Utilisation de l'état local pour stocker la liste de films obtenue depuis l'API et une fonction pour la mettre à jour
  const [movieListApi, setMovieListApi] = useState([]);

  // Options de la requête API, notamment la clé API, la méthode et les en-têtes
  const options = {
    method: "GET",
    headers: {
      accept: "application/json",
      Authorization:
        "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIzODc4ZWExMTkxOTI3ZGRlNjIzNWY3MzAwYzVkMDY5YiIsInN1YiI6IjY1Nzg1ZGVlODlkOTdmMDBjNjc5MDIyYyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.nVCJIptl8KEeB1SZpV5GpNMy_WSBiaPwX0kuqgpbXTk",
    },
  };

  // Effet de côté qui s'exécute lors du montage du composant
  useEffect(() => {
    // Requête à l'API pour obtenir la liste des films populaires
    fetch(
      "https://api.themoviedb.org/3/discover/movie?include_adult=false&include_video=false&language=en-US&page=1&sort_by=popularity.desc",
      options
    )
      .then((response) => response.json()) // Conversion de la réponse en JSON
      .then((movies) => {
        // Mise à jour de la liste de films obtenue depuis l'API
        setMovieListApi(movies.results);

        // Création d'une copie des données stockées dans le local storage
        const localStorageData = {...localStorage};

        // Initialisation d'un tableau vide pour stocker les films à regarder
        let filmsToWatched = [];

        // Boucle sur les clés du local storage
        for (const key in localStorageData) {
          // Vérification si la valeur associée à la clé est "true"
          if (key.startsWith("watched") && localStorageData[key] === "true") {
            // Vérifie qu'on ne récupère que les films true de la liste to watched
            // Extraction de l'ID du film à partir de la clé
            let newKey = key.split(" ");
            newKey = newKey[1]; // garde le deuxieme élément du tableau, soit l'id du film

            // Recherche du film correspondant dans la liste obtenue depuis l'API
            for (const eachMovie of movies.results) {
              if (eachMovie.id == newKey) {
                // Ajout du film à la liste des films à regarder
                filmsToWatched.push(eachMovie);
              }
            }
          }
        }

        // Mise à jour de la liste des films à regarder et du nombre total de films à regarder
        setMovieToWatched(filmsToWatched);
        setTotalMoviesInToWatched(filmsToWatched.length);
      });
  }, []); // Le tableau de dépendances est vide pour exécuter l'effet une seule fois lors du montage

  // Conversion des données obtenues depuis l'API en un tableau
  const data = movieListApi.map((eachData) => eachData);

  return (
    <>
      {/* Conteneur des cartes de films à regarder */}
      {movieToWatched.length !== 0 ? (
        <div className="last-to-watched-container">
          {/* Affichage des quatre derniers films à regarder */}
          {movieToWatched.slice(-4).map((current) => (
            <MovieCard
              title={current.title}
              poster={current.poster_path}
              key={current.id}
              id={current.id}
            />
          ))}
          {/* ET à la fin des 4 cartes, on ajoute la carte "Voir plus" affichant le nombre total de films à regarder */}
          {movieToWatched.length > 4 ? (
            <SeeMoreCard
              number={movieToWatched.length}
              url="/towatchedlist"
            />
          ) : (
            <AddMoreCard />
          )}
        </div>
      ) : (
        <p>Vous n'avez enregistré aucun film dans cette liste.</p>
      )}
    </>
  );
}
