import Footer from "../../components/Footer";
import MainSideBar from "../../components/MainSideBar";
import MovieCard from "../../components/MovieCard";
import ButtonMoveToTop from "../../components/ButtonMoveToTop";
import "./HomePage.css";
import Header from "../../components/Header";
import { useState, useEffect } from "react";
import MainTitle from "../../components/MainTitle";
import { loadMovies } from "../../services/api";

function HomePage() {
  const [movieListApi, setMovieListApi] = useState([]);
  const [movieDetailApi, setmovieDetailApi] = useState([]); //Film avec details
  const [searchBarValue, setSearchBarValue] = useState(""); //valeur de la barre de recherche
  const [searchFilteredMovie, setSearchFilteredMovie] = useState([]); //films filtrés
  const [mainTitle, setMainTitle] = useState("Tous les films");

  const options = {
    method: "GET",
    headers: {
      accept: "application/json",
      Authorization:
        "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIzODc4ZWExMTkxOTI3ZGRlNjIzNWY3MzAwYzVkMDY5YiIsInN1YiI6IjY1Nzg1ZGVlODlkOTdmMDBjNjc5MDIyYyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.nVCJIptl8KEeB1SZpV5GpNMy_WSBiaPwX0kuqgpbXTk",
    },
  };

  useEffect(() => {
    async function callApi(){
      // return loadMovies
      const callMovieList = await loadMovies();
      setmovieDetailApi(callMovieList)
      filter(callMovieList)
      console.log(callMovieList)
    }
    callApi()
    
  }, []);
  const data = movieDetailApi.map((eachData) => eachData);

  console.log(data)


  // FILTRER LES FILMS A PARTIR DE LA BARRE DE RECHERCHE
  useEffect(() => {
    filter(data); //On filtre les données
  }, [searchBarValue]); //A chaque changement de la valeur de la barre de recherche

  const filter = (data) => {
    const filteredMovies = data.filter((eachMovie) => {
      //On filtre sur les données souhaitées
      return eachMovie.title // On retourne le titre du film
        .toLowerCase() // en minuscule
        .includes(searchBarValue.toLowerCase()); // qui inclus la valeur de la barre de recherche
    });
    setSearchFilteredMovie(filteredMovies); //On met à jour le tableau de films filtrés
  };

  // TRIER LES FILMS
  const sortByPopularity = () => {
    const popularSortedMovie = [...searchFilteredMovie].sort(
      (a, b) => b.popularity - a.popularity
    );
    setSearchFilteredMovie(popularSortedMovie);
    changeTitle("Trié par popularité")

  };

  const sortByReleaseDate = (order) => {
    const sortedMovies = [...searchFilteredMovie].sort((a, b) => {
      let releaseDateA = a.release_date ? new Date(a.release_date) : 0;
      let releaseDateB = b.release_date ? new Date(b.release_date) : 0;
      return order === "Les plus récents"
        ? releaseDateB - releaseDateA
        : releaseDateA - releaseDateB;
    });
    setSearchFilteredMovie(sortedMovies);
    changeTitle("Trié par" , order)
  };

  const sortByDuration = (order) => {
    const sortedDurationMovies = [...movieDetailApi].sort((a, b) =>
      order === "Les plus longs" ? b.runtime - a.runtime : a.runtime - b.runtime
    );
    setSearchFilteredMovie(sortedDurationMovies);
    changeTitle("Trié par" , order)
  };

  const sortByRate = (order) => {
    const movieFilterbyRate = [...searchFilteredMovie].sort((a, b) =>
      order === "Les mieux notés"
        ? b.vote_average - a.vote_average
        : a.vote_average - b.vote_average
    );
    setSearchFilteredMovie(movieFilterbyRate);
    changeTitle("Trié par" , order)
  };

  //TRIER PAR CATÉGORIES
  const filterByCategory = (categoryName) => {
    if (categoryName === "All") {
      const showAllMovie = movieDetailApi.map((movie) => movie);
      setSearchFilteredMovie(showAllMovie);
      changeTitle("All");
    } else {
      const filteredMovies = movieDetailApi.filter((movie) => {
        const movieCategories = movie.genres.map((category) => category.name);
        return movieCategories.includes(categoryName);
      });
      setSearchFilteredMovie(filteredMovies);
      changeTitle("Les films de la catégorie " , categoryName)
    }
  };

  //CHANGER LE TITRE PRINCIPAL
  const changeTitle = (title, differentText) => {
    if(title === "All"){
      differentText = "Tous les films";
      setMainTitle(differentText);
    } else if(differentText === "" || differentText === null || differentText === undefined){
      setMainTitle(title);
    } else {
      setMainTitle(`${title} "${differentText}" `);
    }
  };

  return (
    <div className="wrapper">
      <header>
        <Header
          searchBarValue={searchBarValue}
          setSearchBarValue={setSearchBarValue}
        />
      </header>
      <aside>
        <MainSideBar
          sortPopular={sortByPopularity}
          sortByReleaseDate={sortByReleaseDate}
          sortByDuration={sortByDuration}
          sortByRate={sortByRate}
          filterByCategory={filterByCategory}
        />
      </aside>
      <div className="content">
        <h1>
          <MainTitle mainTitle={mainTitle} />
        </h1>
        <section className="container-movies">
          {searchFilteredMovie.map((current) => (
            <MovieCard
              title={current.title}
              poster={current.poster_path}
              duration={current.runtime}
              key={current.id}
              id={current.id}
            />
          ))}
        </section>
        <ButtonMoveToTop />
      </div>

      <footer>
        <Footer />
      </footer>
    </div>
  );
}

export default HomePage;
