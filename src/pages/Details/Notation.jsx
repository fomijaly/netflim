import React, {useState, useEffect} from "react";
import "./Notation.css";

export default function Notation(props) {
  const {movieId} = props; // l'id sert pour retenir en local storage la note par film
  const [rating, setRating] = useState(0);
  const [hover, setHover] = useState(0);

  useEffect(() => {
    // Vérifier si une note est déjà stockée dans le local storage
    const storedRating = localStorage.getItem(`userRating_${movieId}`);
    if (storedRating) {
      setRating(parseInt(storedRating, 10)); // utilise la fonction parseInt pour convertir la valeur stockée dans le localStorage en un nombre entier, puis met à jour l'état de la variable rating.
    }
  }, []); // Tableau vide [] = cet effet s'exécutera uniquement lors du montage du composant

  const handleRating = (index) => {
    // Mise à jour de la note et stockage dans le local storage
    setRating(index);
    localStorage.setItem(`userRating_${movieId}`, index.toString());
  };
  return (
    // Conteneur principal
    <div className="notation-container">
      <p>Noter</p>

      {/* Conteneur pour les étoiles avec une classe dynamique basée sur movieId */}
      <div className={`star-rating-${movieId}`}>
        {/* Utilise un tableau d'étoiles généré dynamiquement avec Array(5) */}
        {[...Array(5)].map((star, index) => {
          // [...Array(5)] créé un tableau contenant 5 éléments (5 undefined pour être précis) à l'aide de Array(5). Ensuite, le spread operator (...) est utilisé pour déployer ces éléments dans un nouvel tableau. Cela équivaut à [undefined, undefined, undefined, undefined, undefined]. On loop ensuite dessus pour le remplir avec les return et un index pour chaque élément.

          // Incrémente l'index pour qu'il commence à 1 au lieu de 0
          index += 1; //chaque étoile aura son propre index de 1 à 5

          // Renvoie un bouton pour chaque étoile
          return (
            <button
              type="button"
              key={index} // on donne à l'étoile son index sous forme de clé
              // Applique une classe dynamique en fonction de l'état de survol et de la note actuelle
              className={index <= (hover || rating) ? "on" : "off"} // les étoiles recevront la classe "on" si leur indice est inférieur ou égal à la note actuelle (rating) ou à l'état de survol (hover).

              //Explication avec exemple : Supposons que la note actuelle (rating) soit égale à 4, et que l'utilisateur survole la quatrième étoile. Dans ce cas, chaque bouton vérifie si son index est inférieur ou égal à la note actuelle (rating) ou à l'état de survol (hover). Pour les boutons de 1 à 4, cette condition est vraie, car 1, 2, 3 et 4 sont inférieurs ou égaux à 4. Ainsi, tous les boutons jusqu'à la quatrième étoile recevront la classe "on" (pour la couleur orange) lorsqu'on survole la quatrième étoile, car ils satisfont tous à la condition de comparaison. C'est pourquoi les trois premières étoiles aussi s'allument.
              
              // Appelle la fonction handleRating lorsqu'une étoile est cliquée
              onClick={() => handleRating(index)}
              // Met à jour l'état de survol lorsque la souris de l'utilisateur survol la zone du bouton/étoile
              onMouseEnter={() => setHover(index)}
              // Réinitialise l'état de survol à la note actuelle lorsqu'une souris quitte le bouton
              onMouseLeave={() => setHover(rating)}>
              {/* Symbole d'étoile Unicode */}
              <span className="star">&#9733;</span>
            </button>
          );
        })}
      </div>
    </div>
  );
}
