import "./Synopsis.css";

export default function Synopsis(props) {
const {synopsis} = props;
  return (
    <div className="detail-movie-synopsis-container">
      <h2 className="detail-movie-synopsis-h2">Synopsis</h2>
      <p>{synopsis}</p>
    </div>
  );
}
