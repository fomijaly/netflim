import "./PresentationMovie.css";
import TagCategory from "./TagCategory";

// URL de base pour les images provenant de l'API TMDB
const beginningUrl = "https://image.tmdb.org/t/p/w500";

export default function PresentationMovie(props) {
  // Extraction des propriétés du composant. tags donnera 3 catégories, comédie, horreur, sf par exemple.
  const {image, title, duration, year, rating, director, actors, tags} = props;

  return (
    // Conteneur principal de la présentation du film
    <div className="presentation-container">
      {/* Image du film */}
      <img
        src={`${beginningUrl}/${image}`} // Utilisation de l'URL de base avec le chemin spécifique de l'image
        alt={`Affiche du film ${title}`} // récupère le titre pour le Alt
        className={`presentation-movie-image`} // Classe CSS pour le style de l'image
      />

      {/* Conteneur des informations textuelles sur le film */}
      <div className="presentation-text-movie-info">
        <h1>{title}</h1>

        {/* Durée du film et année de sortie */}
        <div className="presentation-text-duration-and-year">
          <p>
            <strong>Durée : </strong>
            {duration} minutes
          </p>
          <p>
            <strong>Année : </strong>
            {year}
          </p>
        </div>

        {/* Note du film */}
        <p>
          <strong>Note de la communauté :</strong>{" "}
          {rating.toString().slice(0, 3)} / 10 {/* On ne garde que un chiffre après la virgule */}
        </p>

        {/* Tags du film (composant tag contiendra 3 éléments à maper dessus) */}
        <div className="presentation-movie-tag-container">
          {tags.map((current) => ( //map sur le tableaux de genres [comédie, horreur, thriller] par ex pour tous les afficher avec TagCategory
            <TagCategory tag={current.name} key={current.id} /> // Pour chaque genre le passe en props à TagCategory
          ))}
        </div>
        <div className="presentation-movie-credits">
          {/* Réalisateur du film */}
          <p>
            <strong>Réalisateur : </strong>
            {director}
          </p>

          {/* Acteurs principaux du film */}
          <p>
            <strong>Acteurs : </strong>
            {actors}
          </p>
        </div>
      </div>
    </div>
  );
}
