import React, {useState} from "react";
import "./DetailSideBar.css";
import Notation from "./Notation";
import AddToListDetail from "./AddToListDetail";
import logoEye from "../../assets/logoEye.svg";
import logoEyeOrange from "../../assets/logoEyeOrange.svg";
import logoMarkGray from "../../assets/markgray.svg";
import logoMarkOrange from "../../assets/markorange.svg";
import logoHeart from "../../assets/grayHeart.svg";
import logoHeartOrange from "../../assets/orangeHeart.svg";
import logoShare from "../../assets/share.svg";
import logoShareOrange from "../../assets/shareOrange.svg";

export default function DetailSideBar(props) {
  const {movieId} = props;
  console.log("movieId", movieId);

  // États pour suivre si le film est dans les listes "vus", "à voir", "préférés"
  const [isInWatched, setIsInWatched] = useState(
    localStorage.getItem(`watched ${movieId}`) ===
      "true" /* La clé est composée du type de liste puis de l'identifiant du film (après un _) afin de récupérer les listes par films plus tard */
  );
  //Si la valeur récupérée est "true", cela signifie que le film est déjà marqué comme "à voir", et isInToWatch sera initialisé à true (dans le useState). Dans le cas contraire, si la valeur récupérée n'est pas "true", isInToWatch sera initialisé à false (dans le useState).
  const [isInToWatch, setIsInToWatch] = useState(
    localStorage.getItem(`to-watch ${movieId}`) === "true"
  );
  const [isInFavourites, setIsInFavourites] = useState(
    localStorage.getItem(`favourites ${movieId}`) === "true"
  );

  // Fonctions pour gérer l'ajout/suppression du film dans les listes
  const handleAddToWatched = () => {
    setIsInWatched(!isInWatched); // Bascule l'état actuel du film entre "vu" et "non vu".
    localStorage.setItem(`watched ${movieId}`, (!isInWatched).toString()); //Convertit la valeur booléenne inversée de isInWatched en une chaîne de caractères. Met à jour la valeur correspondante dans le localStorage pour enregistrer le nouveau statut du film.
  };

  const handleAddToToWatch = () => {
    setIsInToWatch(!isInToWatch);
    localStorage.setItem(`to-watch ${movieId}`, (!isInToWatch).toString());
  };

  const handleAddToFavourites = () => {
    setIsInFavourites(!isInFavourites);
    localStorage.setItem(`favourites ${movieId}`, (!isInFavourites).toString());
  };

  return (
    <section className="detail-movie-sidebar">
      {/* Composant pour la notation du film */}
      <Notation movieId={movieId} />

      {/* Appelle le composant en passant des props pour ajouter/retirer le film de la liste "préférés" */}
      <AddToListDetail
        image={isInFavourites ? logoHeartOrange : logoHeart} //Si le film est vu on mets le logo orange sinon le gris
        imageAlt={logoHeartOrange} // Pour passer la logo orange dans le composant, il en a besoin pour le hover
        caption={
          isInFavourites
            ? "Dans mes films préférés"
            : 'Ajouter à la liste "préférés"'
        } // Change le texte à cîté du logo selon si le film est dans les préférés ou non
        title="heart" // Sert juste pour le Alt de l'image dans le composant
        onClick={handleAddToFavourites} // Au click va inverser le statut du film (s'il est true ou false dans cette liste) et va mettre à jour le local storage avec la nouvelle valeur.
      />

      {/* Appelle le composant en passant des props pour ajouter/retirer le film de la liste "vus" */}
      <AddToListDetail
        image={isInWatched ? logoEyeOrange : logoEye}
        imageAlt={logoEyeOrange}
        caption={
          isInWatched ? "Dans mes films déjà vus" : 'Ajouter à la liste "vus"'
        }
        title="eye"
        onClick={handleAddToWatched}
      />

      {/* Appelle le composant en passant des props pour ajouter/retirer le film de la liste "à voir" */}
      <AddToListDetail
        image={isInToWatch ? logoMarkOrange : logoMarkGray}
        imageAlt={logoMarkOrange}
        caption={
          isInToWatch
            ? "Dans mes films à voir"
            : 'Ajouter dans la liste "à voir"'
        }
        title="eye-checked"
        onClick={handleAddToToWatch}
      />

      {/* Composant pour partager le film */}
      <AddToListDetail
        image={logoShare}
        imageAlt={logoShareOrange} // Pour le hover dans le composant
        caption="Partager" // texte affiché
        title="arrow-share" // pour le Alt
      />
    </section>
  );
}
