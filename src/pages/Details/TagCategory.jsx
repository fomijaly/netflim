import "./TagCategory.css"

export default function TagCategory(props) {
  const {tag} = props; // récupère le tag individuel, donc ici ça pourraitêtre "comédie" par ex
  return (
    <p className="tag-category">{tag}</p>
  )
}
