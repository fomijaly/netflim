import React from "react";
import "./Contact.css";
import Header from "../../components/Header";
import Footer from "../../components/Footer";
import GoBack from "../../components/GoBack";

function Contact() {
  return (
    <div>
      <header>
        <Header />
      </header>
      <div className="contact-container">
        <GoBack />
        <h1 className="contact-title">Contactez-nous</h1>

        <p className="contact-paragraph">
          Si vous avez des questions, des commentaires ou des suggestions,
          n'hésitez pas à nous contacter. Remplissez le formulaire ci-dessous et
          nous vous répondrons dès que possible.
        </p>

        <form className="contact-form">
          <label className="contact-label" htmlFor="name">
            Nom :
          </label>
          <input
            className="contact-input"
            type="text"
            id="name"
            name="name"
            required
          />

          <label className="contact-label" htmlFor="email">
            Email :
          </label>
          <input
            className="contact-input"
            type="email"
            id="email"
            name="email"
            required
          />

          <label className="contact-label" htmlFor="message">
            Message :
          </label>
          <textarea
            className="contact-textarea"
            id="message"
            name="message"
            rows="4"
            required></textarea>

          <button className="contact-button" type="submit">
            Envoyer
          </button>
        </form>
      </div>
      <footer>
        <Footer />
      </footer>
    </div>
  );
}

export default Contact;
