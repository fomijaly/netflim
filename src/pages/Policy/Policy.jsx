import React from "react";
import Footer from "../../components/Footer";
import Header from "../../components/Header";
import "./Policy.css";
import GoBack from "../../components/GoBack";

function Policy() {
  return (
    <div>
      <header>
        <Header />
      </header>
      <div className="policy-container">
        <GoBack />
        <h1 className="policy-title">Politiques de confidentialité</h1>

        <p className="policy-paragraph">
          Bienvenue sur Netflim! Netflim attache une grande importance à la
          protection de vos données personnelles et à votre vie privée. La
          présente politique de confidentialité vise à vous informer sur la
          manière dont nous collectons, utilisons, partageons et protégeons vos
          données.
        </p>

        <h2 className="policy-section-title">Collecte des données</h2>

        <p className="policy-paragraph">
          Nous collectons différentes informations lorsque vous utilisez notre
          site, y compris des données d'identification personnelles.
        </p>

        <ul className="policy-list">
          <li className="policy-list-item">Informations sur votre compte</li>
          <li className="policy-list-item">Historique de navigation</li>
          <li className="policy-list-item">Préférences de contenu</li>
        </ul>

        <h2 className="policy-section-title">Utilisation des données</h2>

        <p className="policy-paragraph">
          Les données collectées sont utilisées pour améliorer votre expérience
          sur Netflim et personnaliser le contenu que nous vous proposons.
        </p>

        <p className="policy-paragraph">
          Consultez notre{" "}
          <a className="policy-link" href="/cookies">
            politique relative aux cookies
          </a>{" "}
          pour en savoir plus sur l'utilisation des cookies sur Netflim.
        </p>

        {/* Ajoutez d'autres sections au besoin */}
      </div>
      <footer>
        <Footer />
      </footer>
    </div>
  );
}

export default Policy;
